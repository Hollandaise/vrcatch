# VRCatch
Final Year Project - "Throwing a Ball with a Virtual Robot"

Jacob Holland - University of Leeds

## System Requirements
* Unreal Engine 4.19
* Windows 10 (Creators Update or Later)
* Windows Subsystem for Linux with Ubuntu 18.04 Installed
* ROS Melodic Morenia installed on WSL
* Steam and SteamVR

## Installation Instructions
### Installing Unreal Engine
The Epic Games Installer can be downloaded [Here](https://www.unrealengine.com/en-US/download). Please keep in mind that you will need to create an Epic Games account (Free) to download the appropriate development kit.
When selecting the version of Unreal Engine, please select 4.19, as this is what the project has been tested on. 

### Installing WSL
The Microsoft Docs [(Found Here)](https://docs.microsoft.com/en-us/windows/wsl/install-win10) contain a guide on how to enable WSL in Windows 10. When selecting a distribution, please select Ubuntu, as it is natively supported by ROS, and is what the project has been tested on.
### Setting Up ROS Melodic in WSL
> **IMPORTANT**: Indentical ROS scripts/launch files exists in both the *Dev* and *RoboCatchVR* projects. For this reason, you only need to set up one of the workspaces for each project to work. **Attempting to set up both workspaces may yield unpredictable results**

#### Automated Script
For convinience purposes, a shell script has been included in each project's `catkin_ws` folder in order to install Ros Melodic and compile any necessary source code. To run this script, you will require root access for you WSL user:

```bash
cd <PROJECT_NAME>/catkin_ws
sudo ./init-workspace.sh
```
#### Manual Approach
1. Open a new Ubuntu Terminal and navigate to the desired catkin_ws directory
   ```bash
   cd <PROJECT-NAME>/catkin_ws
   ```
1. Install `ros-melodic-desktop-full` using [The official ROS installation guide](http://wiki.ros.org/melodic/Installation/Ubuntu)
1. Install Additionial RosBridge and TRAC-IK Packages:
    ```bash
   sudo apt-get install ros-melodic-trac-ik ros-melodic-rosbridge-server
   ```
1. Navigate to the `src` folder and initialise the catkin workspace
    ```bash
   cd src/
   source /opt/ros/melodic/setup.bash
   catkin_init_workspace
   ```
1. Return to the root `catkin_ws` directory and run `catkin_make` to compile the necessary source code and set up the necessary directories
    ```bash
   cd ..
   catkin_make
   ```
1. Once the project has successfully compiled you should add the source folder to your bashrc, so the project relevant files can be launched easily
   ```bash
   ESC_CATKIN_ROOT=$( echo "$(pwd)" | sed 's/ /\\ /g' )
   echo "source ${ESC_CATKIN_ROOT}/devel/setup.bash" >> ~/.bashrc
   source ~/.bashrc
   ```

## How to Run
1. Navigate to either the `Dev` or `RoboCatchVR` folder, depending on your preference
1. Launch SteamVR
1. Double-click on the appropriate .uproject file. This should load an instance of the Unreal Engine 4 Editor
1. If you have not launched the project before, You will be prompted to build the project - please choose "Yes"
1. Launch a new instance of the Ubuntu Terminal
1. To launch the Rosbridge server with a single robot, run the master launch file:
   ```bash
   roslaunch ball_catcher baseBot.launch
   ```
1. Click the Play button in the UE4 Editor, the robot should establish communication with the engine.
  > **IMPORTANT**: Always start the server before playing the game, and always stop the game before terminating the server. If communication drops, UE4 may crash.


## Controls
### VR Motion Controls (RoboCatchVR)
![VR Controller Map](IMG/Controller_Map.png "Left - Oculus Touch Controller. Right - HTC Vive Motion Controller")
1. **(Blue) Grip Controls:** Hold to grab an object, release to release an object 
1. **(Red) Movement:** Click/Pressdown to intiate teleport. Aim towards location. Twist to set orientation. Release stick/pad to move.


### KB & Mouse Controls (Dev)
* **W**: Move Forward
* **A**: Strafe Left
* **S**: Move Backwards
* **D**: Strafe Right
* **Mouse**: Aim Weapon
* **Left Click**: Fire Weapon

## Modifying/Adding More Robots
### Changing the Robot Model
> **IMPORTANT**: This project only natively supports the *Continuous* Joint type and only basic mesh types for Links (Cube, Sphere, Cylinder). While support for further joint types are in the pipeline, their behaviours have not been explicitly programmed. **Do So At Your Own Risk**
1. Open your desired project in unreal engine 4 and search for `ROSRobot_BP`

    ![VR Controller Map](IMG/changeRob1.jpg "Left - Find ROSRobot_BP")

2. Click on the "Parser" field and click the 3 dots to select a new URDF file

    ![VR Controller Map](IMG/changeRob2.jpg "Change the absolute file path for the parser in unreal engine")

3. Go to your `catkin_ws/launch` folder and open `baseBot.launch`

    ![VR Controller Map](IMG/changeRob3.jpg "Find the Catkin Launch Directory")

4. Edit the `robot_description` field to the same file as you specified in UE4
   ```xml
     <param name="robot_description" textfile="$(find ball_catcher)/NEWROBOTNAME.urdf" />
   ```

### Adding Additional Robots
> **IMPORTANT**: This feature is experimental and will reduce the performance of all robots. It is not advised you use this feature in it's current state. Additionially, *each new robot needs a unique URDF file with a unique robot name, or movements will synchronise*
1. Open Your Desired Project and Go to the `ROSRobotCatcher` folder, drag another `ROS_RobotBP` onto the map and save.

    ![VR Controller Map](IMG/AddRobot1.jpg "Change the absolute file path for the parser in unreal engine")

2. Find your new ROSRobot_BP instance on the map, and change it's file in the parser field

    ![VR Controller Map](IMG/changeRob2.jpg "Change the absolute file path for the parser in unreal engine")

3. Go to your `catkin_ws/launch` folder and open `additionalBot.launch` (if additional robots are required, just duplicate this file)

    ![VR Controller Map](IMG/changeRob3.jpg "Find the Catkin Launch Directory")

4. Edit the `robot_description` field to the same file as you specified in UE4
   ```xml
     <param name="robot_description" textfile="$(find ball_catcher)/NEWROBOTNAME.urdf" />
   ```
5. Open a new WSL Instance and launch the `basBot.launch` file **THIS MUST ALWAYS BE RUN, AS IT ALSO CONTAINS THE ROSBRIDGE SERVER**
   ```bash
   roslaunch ball_catcher baseBot.launch
   ```
6. For each additional robot you wish to run, open another WSL terminal and launch each additional launch file independently
   ```bash
   roslaunch ball_catcher additionalBot.launch
   ```
7. Play the Game in UE4 as standard.




