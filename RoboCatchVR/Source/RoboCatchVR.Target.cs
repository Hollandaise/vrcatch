// Copyright Jacob Anthony Paul Holland 2019

using UnrealBuildTool;
using System.Collections.Generic;

public class RoboCatchVRTarget : TargetRules
{
	public RoboCatchVRTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "RoboCatchVR" } );
	}
}
