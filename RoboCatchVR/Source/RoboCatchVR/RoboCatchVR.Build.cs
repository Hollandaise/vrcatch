// Copyright Jacob Anthony Paul Holland 2019

using UnrealBuildTool;
using System.IO;
using System;

public class RoboCatchVR : ModuleRules
{
    private string ModulePath
    {
        get { return ModuleDirectory; }
    }

    private String ThirdPartyPath
    {
        get { return Path.GetFullPath(Path.Combine(ModulePath, "../../ThirdParty")); }
    }

    public bool LoadFBX(ReadOnlyTargetRules Target)
    {
        // Libraries Path      
        string LibrariesPath = Path.Combine(ThirdPartyPath, "fbx", "Libraries");
        PublicLibraryPaths.Add(LibrariesPath);
        PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "libfbxsdk-md.lib"));

        // Include path
        string IncludePath = Path.Combine(ThirdPartyPath, "fbx", "Includes");
        PublicIncludePaths.Add(IncludePath);

        // Definitions
        PublicDefinitions.Add(string.Format("WITH_FBX_BINDING=1"));
        return true;
    }

    public bool LoadTinyXML(ReadOnlyTargetRules Target)
    {
        string LibrariesPath = Path.Combine(ThirdPartyPath, "tinyxml2", "Libraries");
        PublicLibraryPaths.Add(LibrariesPath);
        PublicAdditionalLibraries.Add(Path.Combine(LibrariesPath, "tinyxml2.lib"));

        // Include path
        string IncludePath = Path.Combine(ThirdPartyPath, "tinyxml2", "Includes");
        PublicIncludePaths.Add(IncludePath);

        // Definitions
        PublicDefinitions.Add(string.Format("WITH_TINYXML2_BINDING=1"));
        return true;
    }

    public void LoadROSIntegration(ReadOnlyTargetRules Target)
    {
        string rosintegrationPath = Path.GetFullPath(Path.Combine(ModuleDirectory, "../../Plugins/ROSIntegration/Source/ROSIntegration/Private"));
        PrivateIncludePaths.AddRange(
            new string[] {
        rosintegrationPath,
        rosintegrationPath + "/rosbridge2cpp"
            }
        );
        PublicDependencyModuleNames.Add("ROSIntegration");
    }

    public RoboCatchVR(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "Json", "JsonUtilities" });

        PrivateDependencyModuleNames.AddRange(new string[] { });

        LoadFBX(Target);
        LoadTinyXML(Target);
        LoadROSIntegration(Target);

        // Uncomment if you are using Slate UI
        // PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });

        // Uncomment if you are using online features
        // PrivateDependencyModuleNames.Add("OnlineSubsystem");

        // To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
    }
}
