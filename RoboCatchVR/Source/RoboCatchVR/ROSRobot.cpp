// Fill out your copyright notice in the Description page of Project Settings.

#include "ROSRobot.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Components/CapsuleComponent.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "Runtime/Engine/Classes/PhysicsEngine/ConstraintInstance.h"


// Sets default values
AROSRobot::AROSRobot()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Parser_ = CreateDefaultSubobject<UURDFParser>(TEXT("URDF Parser"));
	PhysicsHandleComponent = CreateDefaultSubobject<UPhysicsHandleComponent>(TEXT("Physics Handler"));

	Root_ = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneComponent"));
	Root_->SetupAttachment(this->RootComponent, TEXT("DefaultSceneComponent"));
	this->SetRootComponent(Root_);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> cylinderFinder(TEXT("/Engine/BasicShapes/Cylinder.Cylinder"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> cubeFinder(TEXT("/Engine/BasicShapes/Cube.Cube"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereFinder(TEXT("/Engine/BasicShapes/Sphere.Sphere"));

	if (cylinderFinder.Succeeded()) { CylinderMesh = cylinderFinder.Object; }
	if (cubeFinder.Succeeded()) { CubeMesh = cubeFinder.Object; }
	if (SphereFinder.Succeeded()) { SphereMesh = SphereFinder.Object; }
}


// Called when the game starts or when spawned
void AROSRobot::BeginPlay()
{
	Super::BeginPlay();
	if (Parser_)
	{
		if (Parser_->ParseURDF())
		{
			BuildRobot();
			Catcher_ = NewObject<UCatcher>(Root_, FName("Catcher"));
			Catcher_->RegisterComponent();
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Could not Parse URDF File: %s"), *Parser_->URDF_File.FilePath);
		}
	}
	
	
}

// Called every frame
void AROSRobot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool AROSRobot::RotateJoint(const FString &Name, const float TargetRotation) const
{
	//Unfortunately there is no TPair for component objects - we have to iterate through it manually
	auto Children = GetComponents().Array();
	for (auto C = 0; C < Children.Num(); C++)
	{
		const auto SceneComp = Children[C];
		if (SceneComp->GetName().Equals(Name, ESearchCase::IgnoreCase))
		{
			auto Constraint = Cast<UPhysicsConstraintComponent>(SceneComp);

			FJoint JointToRotate;
			for (auto & Jnt : Joints)
			{
				if (Jnt.Name == Constraint->GetName())
				{
					JointToRotate = Jnt;
					break;
				}
			}

			// Our continuous joints have limits - We need to check the axis of rotation so the behaviour is in line
			// With how ROS perceives it 
			if (JointToRotate.JointAxis.X == 1)
			{
				Constraint->SetAngularOrientationTarget(FRotator(0, 0, TargetRotation));
			}
			else if (JointToRotate.JointAxis.Z == 1)
			{
				Constraint->SetAngularOrientationTarget(FRotator(0, TargetRotation, 0));
			}
			else if (JointToRotate.JointAxis.Y == 1)
			{
				Constraint->SetAngularOrientationTarget(FRotator(TargetRotation,0 , 0));
			}

			return true;
		}

	}
	return false;
}

bool AROSRobot::BuildRobot()
{
	if (Links.Num() <= 1)
	{
		return false;
	}

	auto CurrentJoint = Joints[0];
	auto HighestFound = false;

	//Find the "Base Joint" - the joint where its parent is the base link 
	while (!HighestFound)
	{
		auto HigherExists = false;
		for (auto &Jnt : Joints)
		{
			if (CurrentJoint.ParentLinkName == Jnt.ChildLinkName)
			{
				CurrentJoint = Jnt;
				HigherExists = true;
			}
		}
		if (!HigherExists) { HighestFound = true; }
	}

	//UE_LOG(LogTemp, Warning, TEXT("Highest Joint = %s"), *CurrentJoint.Name)
	//UE_LOG(LogTemp, Warning, TEXT("Highest Link = %s"), *CurrentJoint.ParentLinkName)

	delete BaseNode;
	BaseNode = new FRobNode();

	//The base joint's parent will be the base link
	for (auto &Link: Links)
	{
		if (Link.Name == CurrentJoint.ParentLinkName)
		{
			BaseNode->Link = Link;
			BaseNode->IsBaseLink = true;
			BaseLink = Link.Name;
		}
	}

	BuildTree(BaseNode);

	return RenderModel(BaseNode);
}


void AROSRobot::BuildTree(FRobNode * Node)
{
	if (!Node)
	{
		UE_LOG(LogTemp, Error, TEXT("No node here!"));
		return;
	}

	//Traverse the tree until we have no more children,
	//Base Link == Root, End effector = end leaf
	for (auto &Joint : Joints)
	{
		if (Joint.ParentLinkName.Equals(Node->Link.Name))
		{
			for (auto &Link : Links)
			{
				if (Joint.ChildLinkName.Equals(Link.Name))
				{
					auto ChildNode = new FRobNode();
					ChildNode->Parent = Node;
					ChildNode->Joint = Joint;
					ChildNode->Link = Link;
					Node->Children.Add(ChildNode);
				}
			}
		}
	}

	//For the purpose of a single arm structure, this will work
	//However, this may not work for humanoid models
	//TODO: Find a smarter automated way to calculate the end link
	//Gazebo or RVIZ documentation may have a method for this.
	if (Node->Children.Num() == 0)
	{
		Node->IsEndLink = true;
		EndLink = Node->Link.Name;
	}

	for (auto &Child : Node->Children)
	{
		BuildTree(Child);
	}
}

bool AROSRobot::RenderModel(FRobNode * Node)
{
	if (!Node) { 
		UE_LOG(LogTemp, Error, TEXT("No node here!"));
		return false; 
	}
	
	Links.Empty();

	const auto Link = &Node->Link;
	const auto Joint = &Node->Joint;
	const auto BUseVisual = Link->isVisual;
	const auto BUseCollision = Link->isCollision;
	UE_LOG(LogTemp, Warning, TEXT("Current Link = %s"), *Link->Name)
	
	USceneComponent * ParentComponent;
	UPrimitiveComponent * ParentLink = nullptr;
	//FVector LinkOriginLocation(0);

	if (Node->IsBaseLink)
	{
		ParentComponent = RootComponent;
	}
	else
	{
		ParentComponent = Cast<USceneComponent>(LinkComponents.FindRef(Node->Parent->Link.Name));
		ParentLink = LinkComponents.FindRef(Node->Parent->Link.Name);
	}

	if (LinkComponents.Contains(Link->Name))
	{
		//Can't have a robot with identical link.joint names
		return false;
	}

	//Lets start by concerning only with the visual aspect - Collision meshes might be 
	//skewed depending on how we generate the configuration space of the arm
	UStaticMesh * Mesh = nullptr;
	UStaticMeshComponent * MeshComponent = nullptr;
	UShapeComponent * ShapeComponent = nullptr;
	const auto Scale(Link->VisualMeshScale);
	auto MeshName = Link->Name;

	if (BUseVisual || BUseCollision)
	{
		switch (Link->VisualMeshType)
		{//switch start
			case EMeshType::BOX:
			{//Box Start
				Mesh = CubeMesh;
				ShapeComponent = NewObject<UBoxComponent>(Root_, FName(*MeshName));
				auto BoxSize(Scale);
				BoxSize *= 50.f;
				Cast<UBoxComponent>(ShapeComponent)->InitBoxExtent(BoxSize);
				Cast<UBoxComponent>(ShapeComponent)->SetBoxExtent(BoxSize);
				UE_LOG(LogTemp, Warning, TEXT("Box Found!"));
				break;
			}//Box End

			case EMeshType::CYLINDER:
			{//Cylinder Start
				Mesh = CylinderMesh;
				ShapeComponent = NewObject<UCapsuleComponent>(Root_, FName(*MeshName));
				const auto Radius = Scale.X * 50.f;
				const auto Height = Scale.Y * 50.f;
				Cast<UCapsuleComponent>(ShapeComponent)->InitCapsuleSize(Radius, Height);
				Cast<UCapsuleComponent>(ShapeComponent)->SetCapsuleSize(Radius, Height);
				UE_LOG(LogTemp, Warning, TEXT("Cylinder Found!"));
				break;
			}//Cylinder End

			case EMeshType::SPHERE:
			{//Sphere Start
				Mesh = SphereMesh;
				ShapeComponent = NewObject<USphereComponent>(Root_, FName(*MeshName));
				const auto Radius = Scale.X * 50.f;
				Cast<USphereComponent>(ShapeComponent)->InitSphereRadius(Radius);
				Cast<USphereComponent>(ShapeComponent)->SetSphereRadius(Radius);
				UE_LOG(LogTemp, Warning, TEXT("Sphere Found!"));
				break;
			}//Sphere End

			//TODO: Use FBX Libraries (Included in Builds to Properly Import .DAE->.FBX)
			case EMeshType::MESH:
			{//Mesh Start
				//This bit is going to be quite frankly, A nightmare
				//Check for file extensions.
				//Allow .dae files -> use AutoDesk SDK to convert to .fbx first -> return new path and reassign in tree.
				Mesh = CubeMesh;
				ShapeComponent = NewObject<UBoxComponent>(Root_, FName(*MeshName));
				const auto BoxSize(Scale);
				Cast<UBoxComponent>(ShapeComponent)->InitBoxExtent(BoxSize);
				Cast<UBoxComponent>(ShapeComponent)->SetBoxExtent(BoxSize);
				UE_LOG(LogTemp, Warning, TEXT("Mesh Found!"));
				break;
			}//MESH end

			default:
				break;
		}//switch end
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Link \"%s\" has no visual or collision options enabled - cannot render this object"), *(Link->Name))
	}

	if (ShapeComponent)
	{
		MeshName += "_VIS";
		MeshComponent = NewObject<UStaticMeshComponent>(ShapeComponent, FName(*MeshName));
	}
	else
	{
		MeshComponent = NewObject<UStaticMeshComponent>(ParentComponent, FName(*MeshName));
	}

	// Add TF Tags Like in RViz - These could prove useful for MoveIt! Intergration.
	if (Node->IsBaseLink) {
		const auto TfTag = FString::Printf(TEXT("TF;ParentFrameId,%s;ChildFrameId,%s;"), *ParentComponent->GetName(), *Link->Name);
		MeshComponent->ComponentTags.Add(FName(*TfTag));
	}
	else {
		const auto TfTag = FString::Printf(TEXT("TF;ChildFrameId,%s;"), *Link->Name);
		MeshComponent->ComponentTags.Add(FName(*TfTag));
	}


	MeshComponent->SetStaticMesh(Mesh);
	MeshComponent->SetSimulatePhysics(true);
	MeshComponent->RegisterComponent();

	FVector MinBounds, MaxBounds;
	MeshComponent->GetLocalBounds(MinBounds, MaxBounds);

	auto CollisionLocation = Link->CollisionMeshLocation;
	auto VisualLocation = Link->VisualLocation;
	CollisionLocation *= (MaxBounds * 2);
	VisualLocation *= (MaxBounds * 2);

	if (ShapeComponent)
	{
		if (Link->InertialMass > 0)
		{
			ShapeComponent->SetMassOverrideInKg(FName("ShapeComponent"), Link->InertialMass, true);
		}

		//Not letting the base joint move initially stops it flying into oblivion/falling through the world if it falls over
		//This is primarily an issue with the weight distribution from the basic robot design
		if (Node->IsBaseLink)
		{
			ShapeComponent->SetSimulatePhysics(false);
		}
		else
		{
			ShapeComponent->SetSimulatePhysics(true);
		}

		ShapeComponent->SetCollisionProfileName("RobotModel");
		ShapeComponent->SetCollisionEnabled(ECollisionEnabled::Type::QueryAndPhysics);

		//Make sure the collision boundaries the the visual mesh are in the same location
		ShapeComponent->RegisterComponent();
		MeshComponent->WeldTo(ParentComponent);
		ShapeComponent->SetRelativeScale3D(FVector(1, 1, 1));
		ShapeComponent->SetWorldLocation(ParentComponent->GetComponentLocation());
		ShapeComponent->SetWorldRotation(ParentComponent->GetComponentRotation());
		ShapeComponent->AddLocalOffset(CollisionLocation);
		ShapeComponent->AddLocalRotation(Link->CollisionMeshRotation);

		MeshComponent->SetSimulatePhysics(false);
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		MeshComponent->SetCollisionObjectType(ECC_Visibility);
		MeshComponent->WeldTo(ShapeComponent);
		MeshComponent->SetWorldLocation(ParentComponent->GetComponentLocation());
		MeshComponent->SetWorldRotation(ParentComponent->GetComponentRotation());
		MeshComponent->AddLocalOffset(VisualLocation);
		MeshComponent->AddLocalRotation(Link->VisualRotation);
		MeshComponent->bHiddenInGame = false;
		MeshComponent->SetWorldScale3D(Scale);
	}
	else
	{
		if (Link->InertialMass > 0)
		{
			MeshComponent->SetMassOverrideInKg(NAME_None, Link->InertialMass, true);
		}
		
		ShapeComponent->SetCollisionObjectType(ECC_GameTraceChannel2);
		ShapeComponent->SetCollisionResponseToAllChannels(ECR_Block);
		ShapeComponent->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECR_Ignore);
		MeshComponent->SetWorldLocation(ParentComponent->GetComponentLocation());
		MeshComponent->SetWorldRotation(ParentComponent->GetComponentRotation());
		MeshComponent->AddRelativeLocation(VisualLocation);
		MeshComponent->SetRelativeRotation(Link->VisualRotation);
	}


	

	if (!Node->IsBaseLink)
	{
		auto ConstraintComponent = NewObject<UPhysicsConstraintComponent>(ParentComponent, FName(*Joint->Name));
		auto ConstraintInstance = SetConstraintInstance(Joint);
		ConstraintInstance.ProfileInstance.TwistLimit.bSoftConstraint = false;
		ConstraintInstance.ProfileInstance.ConeLimit.bSoftConstraint = false;

		FRotator RotTarget(0.f, 0.f, 0.f);

		// Set Mechanical Constraints for our Joints to make sure they maintain their position when spawned
		// Otherwise all joints would ragdoll.
		ConstraintInstance.ProfileInstance.AngularDrive.AngularVelocityTarget = FVector(0.f, 0.f, 0.f);
		ConstraintInstance.ProfileInstance.AngularDrive.SlerpDrive.bEnableVelocityDrive = true;
		ConstraintInstance.ProfileInstance.AngularDrive.SwingDrive.bEnableVelocityDrive = true;
		ConstraintInstance.ProfileInstance.AngularDrive.TwistDrive.bEnableVelocityDrive = true;


		ConstraintInstance.ProfileInstance.AngularDrive.AngularDriveMode = EAngularDriveMode::TwistAndSwing;
		ConstraintInstance.SetLinearXLimit(LCM_Locked, 0);
		ConstraintInstance.SetLinearYLimit(LCM_Locked, 0);
		ConstraintInstance.SetLinearZLimit(LCM_Locked, 0);

		ConstraintInstance.ProfileInstance.AngularDrive.OrientationTarget = RotTarget;

		ConstraintInstance.ProfileInstance.AngularDrive.SlerpDrive.bEnablePositionDrive = true;
		ConstraintInstance.ProfileInstance.AngularDrive.SwingDrive.bEnablePositionDrive = true;
		ConstraintInstance.ProfileInstance.AngularDrive.TwistDrive.bEnablePositionDrive = true;

		// ================ HERE BE DRAGONS ========================= //
		//The issue with utilizing Physics Constraint Components is that their behaviour is predominantly "spring-based"
		//The were designed for objects such as pendulums/car suspension in mind - NOT rigid-body motors
		//This means we need a new type of rigid constraint without relying on a skeletal mesh (as joint mass can't be individually set that way/hard to automate)
		//TODO: Create a new joint object in order to create rigid motor behaviour - This may require Understanding the Physics Engine and inheriting certain behaviours from bone constraints
		ConstraintInstance.ProfileInstance.AngularDrive.SlerpDrive.Stiffness = ShapeComponent->GetMass() * 120000;
		ConstraintInstance.ProfileInstance.AngularDrive.SwingDrive.Stiffness = ShapeComponent->GetMass() * 120000;
		ConstraintInstance.ProfileInstance.AngularDrive.TwistDrive.Stiffness = ShapeComponent->GetMass() * 120000;

		ConstraintComponent->ConstraintInstance = ConstraintInstance;
		ConstraintComponent->SetDisableCollision(false);
		ConstraintComponent->AttachToComponent(ParentComponent, FAttachmentTransformRules(EAttachmentRule::KeepRelative, false));
		ConstraintComponent->RegisterComponent();
		ConstraintComponent->ApplyWorldOffset(FVector(0), false);

		if (ShapeComponent)
		{
			
			ShapeComponent->AddRelativeLocation(Joint->JointLocation);
			ShapeComponent->AddRelativeRotation(Joint->JointRotator);
			//LinkOriginLocation = ShapeComponent->GetRelativeTransform().GetLocation();
		}
		else
		{
			MeshComponent->AddRelativeLocation(Joint->JointLocation);
			MeshComponent->AddRelativeRotation(Joint->JointRotator);
			//LinkOriginLocation = MeshComponent->GetRelativeTransform().GetLocation();
		}

		ConstraintComponent->SetRelativeRotation(Joint->JointRotator);
		ConstraintComponent->ConstraintActor1 = this;
		ConstraintComponent->ConstraintActor2 = this;

		if (ShapeComponent)
		{
			ConstraintComponent->SetWorldLocation(ShapeComponent->GetComponentLocation());
			ConstraintComponent->SetConstrainedComponents(ParentLink, NAME_None, ShapeComponent, NAME_None);
		}
		else
		{
			ConstraintComponent->SetWorldLocation(ShapeComponent->GetComponentLocation());
			ConstraintComponent->SetConstrainedComponents(ParentLink, NAME_None, MeshComponent, NAME_None);
		}

		//Set The Transform for each rotation
		//May be needed for MoveIt! Integration
		//auto ParentRotation = ParentLink->GetComponentRotation();
		//auto ChildRotation = ShapeComponent ? (ShapeComponent->GetComponentRotation()) : (MeshComponent->GetComponentRotation());
		//auto InitialRotationQuaternion = FQuat(ParentRotation).Inverse() * FQuat(ChildRotation);

		//OriginRotations.Add(Joint->Name, InitialRotationQuaternion);
		JointComponents.Add(Joint->Name, ConstraintComponent);
	}
	if (ShapeComponent)
	{
		LinkComponents.Add(Link->Name, ShapeComponent);
	}
	else
	{
		LinkComponents.Add(Link->Name, MeshComponent);
	}

	for (auto &Child : Node->Children)
	{
		RenderModel(Child);
	}

	return true;
	//Get each link, check name and position,
	//Proceed to render everything as a box for the time being, we can add custom parameters/textures later.
}


FConstraintInstance AROSRobot::SetConstraintInstance(FJoint * Joint)
{
	// Create a basic fixed constraint
	FConstraintInstance ConstraintInstance;

	// Default constraint for a FIXED joint
	ConstraintInstance.SetDisableCollision(true);
	ConstraintInstance.SetLinearXLimit(LCM_Locked, 0);
	ConstraintInstance.SetLinearYLimit(LCM_Locked, 0);
	ConstraintInstance.SetLinearZLimit(LCM_Locked, 0);
	ConstraintInstance.SetAngularSwing1Limit(ACM_Locked, 0);
	ConstraintInstance.SetAngularSwing2Limit(ACM_Locked, 0);
	ConstraintInstance.SetAngularTwistLimit(ACM_Locked, 0);
	ConstraintInstance.AngularRotationOffset = FRotator(0, 0, 0);

	const auto AngularConstraintMotion = ACM_Free;
	ConstraintInstance.ProfileInstance.AngularDrive.AngularDriveMode = EAngularDriveMode::TwistAndSwing;

	switch (Joint->JointType)
	{
		//TODO: Add More Joint Types
		case EJointType::CONTINUOUS:
		{
			const auto JointAxis = Joint->JointAxis;
			const auto SimpleLimit = 0.0f;
			if (JointAxis.X == 1) {
				ConstraintInstance.SetAngularTwistLimit(AngularConstraintMotion, SimpleLimit);
				ConstraintInstance.SetAngularSwing1Limit(ACM_Limited, 0);
				ConstraintInstance.SetAngularSwing2Limit(ACM_Limited, 0);
				ConstraintInstance.ProfileInstance.AngularDrive.AngularDriveMode = EAngularDriveMode::SLERP;
				ConstraintInstance.ProfileInstance.AngularDrive.SlerpDrive.MaxForce = Joint->LimitEffort;
				ConstraintInstance.ProfileInstance.AngularDrive.SlerpDrive.bEnablePositionDrive = true;
			}
			if (JointAxis.Y == 1) {
				ConstraintInstance.SetAngularSwing2Limit(AngularConstraintMotion, SimpleLimit);
				ConstraintInstance.ProfileInstance.AngularDrive.SlerpDrive.MaxForce = Joint->LimitEffort;
				ConstraintInstance.ProfileInstance.AngularDrive.SwingDrive.bEnablePositionDrive = true;
			}
			if (JointAxis.Z == 1) {
				ConstraintInstance.SetAngularSwing1Limit(AngularConstraintMotion, SimpleLimit);
				ConstraintInstance.ProfileInstance.AngularDrive.SlerpDrive.MaxForce = Joint->LimitEffort;
				ConstraintInstance.ProfileInstance.AngularDrive.SwingDrive.bEnablePositionDrive = true;
			}
			ConstraintInstance.AngularRotationOffset = FRotator(0, 0, 0);
			return ConstraintInstance;
		}

		default:
			return ConstraintInstance;
	}
	
}
