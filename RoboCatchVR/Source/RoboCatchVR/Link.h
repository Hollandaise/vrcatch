// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Core/Public/Containers/UnrealString.h"
#include "Runtime/Core/Public/Math/Vector.h"
#include "Runtime/Core/Public/Math/Rotator.h"
#include "Link.generated.h"


UENUM(BlueprintType)
enum class EMeshType : uint8 {
	BOX	UMETA(DisplayName = "Box"),
	CYLINDER UMETA(DisplayName = "Cylinder"),
	SPHERE UMETA(DisplayName = "Sphere"),
	MESH UMETA(DisplayName = "Mesh"),
	UNKNOWN = 255 UMETA(DisplayName = "Unknown")
};

USTRUCT(BlueprintType)
struct FLink
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector InertialLocation = FVector(0.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRotator InertialRotation = FRotator(0.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float InertialMass = -1; //This should be changed
		//Will not worry about inertial matrix for the time being

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isVisual = false;

		//Visual Parameters (location/geometry)
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector VisualLocation = FVector(0.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRotator VisualRotation = FRotator(0.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EMeshType VisualMeshType = EMeshType::UNKNOWN;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector VisualMeshScale; //Will remain undefined until we have the meshtype declared

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString VisualMeshPath = ""; //If we have a mesh path needing to be included/possibly converted to FBX


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isCollision = false;

		//Collision Parameters
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector CollisionMeshLocation = FVector(0.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRotator CollisionMeshRotation = FRotator(0.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EMeshType CollisionMeshType = EMeshType::UNKNOWN;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector CollisionMeshScale;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString CollisionMeshPath = "";
};