// Copyright Jacob Anthony Paul Holland 2019

#include "RoboCatchVR.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, RoboCatchVR, "RoboCatchVR" );
