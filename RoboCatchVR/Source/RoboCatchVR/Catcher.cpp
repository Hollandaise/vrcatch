// Fill out your copyright notice in the Description page of Project Settings.

#include "Catcher.h"
#include "ROSRobot.h"
#include "Runtime/Core/Public/Templates/SharedPointer.h"
#include "Runtime/Json/Public/Dom/JsonObject.h"
#include "Runtime/Json/Public/Serialization/JsonSerializer.h"


void UCatcher::InitSubscriber()
{
	CatcherSubscriber_ = NewObject<UTopic>(StaticClass());
	if (!CatcherSubscriber_)
	{
		return;
	}
	const FString SubName = "/" + Owner_->RobotName + "/vr_ik_response";
	CatcherSubscriber_->Init(RosInstance_->ROSIntegrationCore, *SubName, TEXT("std_msgs/String"));

	const std::function<void(TSharedPtr<FROSBaseMsg>)> SubscribeCallback = [&](const TSharedPtr<FROSBaseMsg> Msg) -> void
	{
		auto Concrete = StaticCastSharedPtr<ROSMessages::std_msgs::String>(Msg);
		const FString Data = Concrete->_Data;
		UE_LOG(LogTemp, Warning, TEXT("I Heard %s"), *Data);
		if (Concrete.IsValid() && !SolutionAvailable_)
		{
			SolutionAvailable_ = true;
			ProcessCatch(Concrete->_Data);
		}
	};

	CatcherSubscriber_->Subscribe(SubscribeCallback);

}


void UCatcher::ProcessCatch(const FString & Data) const
{
	UE_LOG(LogTemp, Error, TEXT("Incoming string was: %s"), *Data);
	const TSharedRef<TJsonReader<TCHAR>> JsonReader = FJsonStringReader::Create(*Data);
	TSharedPtr<FJsonObject> JsonObject = MakeShareable(new FJsonObject());
	FJsonSerializer::Deserialize(JsonReader, JsonObject);
	
	for (auto CurrentJsonValue = JsonObject->Values.CreateConstIterator(); CurrentJsonValue; ++CurrentJsonValue)
	{
		const auto Name = (*CurrentJsonValue).Key;
		const auto Value = (*CurrentJsonValue).Value;
		const auto ToDegrees = FMath::RadiansToDegrees(Value->AsNumber());

		if (Owner_)
		{
			const auto Result = Owner_->RotateJoint(Name, ToDegrees);
			if (!Result)
			{
				UE_LOG(LogTemp, Error, TEXT("Rotation Unsuccessful"))
			}
		}

	}
}


void UCatcher::InitPublisher()
{
	CatcherPublisher_ = NewObject<UTopic>(StaticClass());
	if (!CatcherPublisher_)
	{
		UE_LOG(LogTemp, Error, TEXT("Rotation Unsuccessful"))
		return;
	}
	FString PubName = "/" + Owner_->RobotName + "/catch_transformer";
	CatcherPublisher_->Init(RosInstance_->ROSIntegrationCore, *PubName, TEXT("geometry_msgs/Vector3"), 1000);
	CatcherPublisher_->Advertise();
}


// Sets default values for this component's properties
UCatcher::UCatcher()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;


}


// Called when the game starts
void UCatcher::BeginPlay()
{
	Super::BeginPlay();
	RosInstance_ = Cast<UROSIntegrationGameInstance>(GetOwner()->GetWorld()->GetGameInstance());
	if (RosInstance_)
	{
		Owner_ = Cast<AROSRobot>(GetOwner());
		if (!Owner_)
		{
			UE_LOG(LogTemp, Error, TEXT("Could not find a ROS Component"))
		}
		else
		{
			InitPublisher();
			InitSubscriber();
		}
		

		
	}
}


// Called every frame
void UCatcher::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}


void UCatcher::CanCatch(const FTransform &Transform, bool &Possible)
{
	//UE_LOG(LogTemp, Warning, TEXT("Current Transform = %s"), *(transform.ToString()))
	UE_LOG(LogTemp, Error, TEXT("Catch Called"))

	if (CatcherPublisher_)
	{
		if (!SolutionAvailable_)
		{
			const auto Xyz = Transform.GetLocation();
			UE_LOG(LogTemp, Error, TEXT("%s"), *Xyz.ToString())


			const FVector OutputVector((Xyz.X / 100), -(Xyz.Y / 100), -(Xyz.Z / 100));
			const TSharedPtr<ROSMessages::geometry_msgs::Vector3> VecMessage(
				new ROSMessages::geometry_msgs::Vector3(OutputVector));
			CatcherPublisher_->Publish(VecMessage);
		}
	}
	Possible = SolutionAvailable_;
}

