// Copyright Jacob Anthony Paul Holland 2019

using UnrealBuildTool;
using System.Collections.Generic;

public class RoboCatchVREditorTarget : TargetRules
{
	public RoboCatchVREditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "RoboCatchVR" } );
	}
}
