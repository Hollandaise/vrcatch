#include "ball_catcher.hpp"

BallCatcher::BallCatcher(ros::NodeHandle &nh,std::string cs, std::string ce, double t, std::string u, const std::string &robot_name)
{

    chain_start = cs;
    chain_end = ce;
    timeout = t;
    urdf_param = u;
    
    std::string pub_name = "/" + robot_name + "/vr_ik_response";
    std::string sub_name = "/" + robot_name + "/catch_transformer";
    catch_publisher = nh.advertise<std_msgs::String>(pub_name, 1000);
    sub = nh.subscribe(sub_name, 1000, &BallCatcher::checkForSolution, this);

    solver = new TRAC_IK::TRAC_IK(chain_start, chain_end, urdf_param, timeout, eps);

    bool valid = solver->getKDLChain(chain);

    if (!valid)
    {
        throw std::runtime_error("There was no valid KDL chain found");
    }

    valid = solver->getKDLLimits(ll, ul);

    if (!valid)
    {
        throw std::runtime_error("There were no valid KDL joint limits found");
    }

    assert(chain.getNrOfJoints() == ll.data.size());
    assert(chain.getNrOfJoints() == ul.data.size());

    ROS_INFO("Using %d joints", chain.getNrOfJoints());

    nominal = KDL::JntArray(chain.getNrOfJoints());

    for (uint j=0; j<nominal.data.size(); j++) {
        nominal(j) = (ll(j)+ul(j))/2.0;
    } 
}

void BallCatcher::checkForSolution(const geometry_msgs::Vector3 &msg)
{
    // ROS_INFO("I heard: [%f, %f, %f]", msg.x, msg.y, msg.z);

        KDL::Vector desired_pose(msg.x, msg.y, msg.z);
        KDL::Frame end_effector_pose(desired_pose);

        KDL::JntArray result;

        if (solver->CartToJnt(nominal, end_effector_pose, result) >= 0)
        {
            Json::Value root;
            // std::string solution = "Found it!";
            ROS_INFO_STREAM("Solution Found!");
            for(int i = 0; i < result.data.size(); i++)
            {
                root[chain.getSegment(i).getJoint().getName()] = result(i);
            }
            Json::StreamWriterBuilder builder;
            builder["indentation"] = "";
            const std::string output = Json::writeString(builder, root);
            std_msgs::String toSend;
            toSend.data = output;
            catch_publisher.publish(toSend);
        }
        // else{
        //     ROS_INFO_STREAM("Nope");
        // }
        
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "catch_proc");
    ros::NodeHandle nh("~");

    std::string chain_start, chain_end, urdf_param;
    double timeout;

    nh.param("chain_start", chain_start, std::string(""));
    nh.param("chain_end", chain_end, std::string(""));

    if (chain_start == "" || chain_end == "")
    {
        ROS_FATAL("Missing chain info in launch file");
        exit(-1);
    }

    nh.param("timeout", timeout, 0.005);
    nh.param("urdf_param", urdf_param, std::string(""));
    




    if (urdf_param == "")
    {
        ROS_FATAL("URDF File Is Empty! Aborting...");
        exit(-1);
    }

    urdf::Model model;

    if(!model.initParam(urdf_param))
    {
        throw std::runtime_error("There was no valid KDL chain found");
    }

    try
    {
        BallCatcher catcher(nh,chain_start, chain_end, timeout, urdf_param, model.getName());
        ROS_INFO("Spinning!");

        ros::spin();
    }
    catch (std::exception &e)
    {
        ROS_FATAL("The Robot Failed To Initialize");
        exit(-1);
    }

    

    return 0;
}
