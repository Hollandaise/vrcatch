#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please Run As Root";
  exit  
fi


if [ $(dpkg-query -W -f='${Status}' ros-melodic-desktop-full 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "Installing ROS Melodic";
  sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list';
  apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116;
  apt update && apt install -y ros-melodic-desktop-full;
fi

if [ $(dpkg-query -W -f='${Status}' ros-melodic-rosbridge-server 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "Installing RosBridge Server";
  apt install -y ros-melodic-rosbridge-server;
fi

if [ $(dpkg-query -W -f='${Status}' ros-melodic-trac-ik 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "Installing TRAC-IK";
  apt install -y ros-melodic-trac-ik;
fi

CATKIN_ROOT=$(pwd);
echo $CATKIN_ROOT
source /opt/ros/melodic/setup.bash;

if [ -f "$CATKIN_ROOT/src/CMakeLists.txt" ]
then
    echo "File exists";
else
  cd "$CATKIN_ROOT/src" && catkin_init_workspace;
fi

echo "Building Source Code...";
cd "$CATKIN_ROOT" && catkin_make;

ESC_CATKIN_ROOT=$( echo "$CATKIN_ROOT" | sed 's/ /\\ /g' )
echo "$ESC_CATKIN_ROOT"
# From this point init workspace and run with it
while true; do
    read -p "All Done! Would You Like To Permanently Add The Appropriate Launch Commands To Your .bashrc file? [Default is NO]: " yn
    case $yn in
        [Yy]* ) echo "source ${ESC_CATKIN_ROOT}/devel/setup.bash" >> ~/.bashrc; break;;
        [Nn]* ) break;;
        * ) break;;
    esac
done

echo "Setup Complete - Please Restart Your Terminal Instance."



