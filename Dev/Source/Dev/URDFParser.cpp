// Fill out your copyright notice in the Description page of Project Settings.



#include "URDFParser.h"
#include "ROSRobot.h"
#include <string>
#include "Runtime/Core/Public/Misc/CString.h"





// Sets default values for this component's properties
UURDFParser::UURDFParser()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	this->bEditableWhenInherited = true;

	// ...
}

//TODO: Complete This Method
FVector UURDFParser::GenerateVectors(FString &S)
{
	//Split string into 3 floats via a space
	//Atof the 3 values provided
	TArray<FString> vals;
	S.ParseIntoArray(vals, TEXT(" "));
	const auto X = FCString::Atof(*vals[0]);
	const auto Y = FCString::Atof(*vals[1]);
	const auto Z = FCString::Atof(*vals[2]);
	//UE_LOG(LogTemp, Warning, TEXT("Vector Values %f %f %f"), X, Y, Z);
	return FVector(X,Y,Z);
}

//TODO: Implement This Method
FRotator UURDFParser::GenerateRotators(FString &S)
{
	//Split string into 3 floats via a space
	//Atof the 3 values provided
	TArray<FString> Vals;
	S.ParseIntoArray(Vals, TEXT(" "));
	float R = FMath::RadiansToDegrees(FCString::Atof(*Vals[0]));
	float P = FMath::RadiansToDegrees(FCString::Atof(*Vals[1]));
	float Y = FMath::RadiansToDegrees(FCString::Atof(*Vals[2]));

	//RPY in ROS is PYR in ROS due to change in co-ordinate system
	return FRotator(P,Y,R);
}

FVector UURDFParser::GenerateCylinderScale(FString &Rad, FString &Len)
{
	const auto Radius = FCString::Atof(*Rad) * 2.f;
	const auto Length = FCString::Atof(*Len);
	return FVector(Radius, Radius, Length);
}

FVector UURDFParser::GenerateSphereScale(FString &Rad)
{
	const auto Radius = FCString::Atof(*Rad) * 2.f;
	return FVector(Radius, Radius, Radius);
}


bool UURDFParser::ParseLink(tinyxml2::XMLElement * El, FLink &Link)
{
	const FString LinkName = El->Attribute("name");
	//UE_LOG(LogTemp, Error, TEXT("Name for link = %s"), *linkName);
	Link.Name = LinkName;

	//Set up Shape Parameters as collision volumes might differ
	if (El->FirstChildElement("visual"))
	{
		const auto Params = SetMeshAndScale(El->FirstChildElement("visual"));
		//Invalid shape type
		Link.VisualMeshType = Params.Key;
		Link.VisualMeshScale = Params.Value;
		if (Link.VisualMeshType == EMeshType::UNKNOWN)
		{
			UE_LOG(LogTemp, Error, TEXT("Could not find appropriate visual mesh type"));
			return false;
		}
		if (Link.VisualMeshType == EMeshType::MESH)
		{
			if (!El->FirstChildElement("visual")->FirstChildElement("geometry")->FirstChildElement("mesh")->Attribute("filename"))
			{
				//We need a file for this mesh!!!
				UE_LOG(LogTemp, Warning, TEXT("No mesh file has been provided for this link"));
				return false;
			}
			Link.VisualMeshPath = El->FirstChildElement("visual")->FirstChildElement("geometry")->FirstChildElement("mesh")->Attribute("filename");
		}

		if (El->FirstChildElement("visual")->FirstChildElement("origin"))
		{
			FString Loc = El->FirstChildElement("visual")->FirstChildElement("origin")->Attribute("xyz");
			FString Rot = El->FirstChildElement("visual")->FirstChildElement("origin")->Attribute("rpy");

			Link.VisualLocation = GenerateVectors(Loc);
			Link.VisualRotation = GenerateRotators(Rot);
		}
		Link.isVisual = true;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Could not find appropriate mesh type for link"));
		return false;
	}

	//Collision Mesh Is Not Required, but is default
	if (El->FirstChildElement("collision"))
	{
		const auto Params = SetMeshAndScale(El->FirstChildElement("collision"));
		Link.CollisionMeshType = Params.Key;
		Link.CollisionMeshScale = Params.Value;
		if (Link.CollisionMeshType == EMeshType::UNKNOWN)
		{
			UE_LOG(LogTemp, Error, TEXT("Could not find appropriate collision mesh type"));
			return false;
		}
		if (Link.CollisionMeshType == EMeshType::MESH)
		{
			Link.CollisionMeshPath = El->FirstChildElement("collision")->FirstChildElement("geometry")->FirstChildElement("mesh")->Attribute("filename");
		}

		if (El->FirstChildElement("collision")->FirstChildElement("origin"))
		{
			FString Loc = El->FirstChildElement("collision")->FirstChildElement("origin")->Attribute("xyz");
			FString Rot = El->FirstChildElement("collision")->FirstChildElement("origin")->Attribute("rpy");

			Link.CollisionMeshLocation = GenerateVectors(Loc);
			Link.CollisionMeshRotation = GenerateRotators(Rot);
			Link.isCollision = true;
		}
	} // Collision Parameters are not required, don't throw an error here...


	//Inertial Checks
	
	if (El->FirstChildElement("inertial"))
	{
		auto Inertial = El->FirstChildElement("inertial");
		if (Inertial->FirstChildElement("origin"))
		{
			if (Inertial->FirstChildElement("origin")->Attribute("xyz"))
			{
				FString Loc = Inertial->FirstChildElement("origin")->Attribute("xyz");
				Link.InertialLocation = GenerateVectors(Loc);

			}
			if (Inertial->FirstChildElement("origin")->Attribute("rpy"))
			{
				FString Rot = Inertial->FirstChildElement("origin")->Attribute("rpy");
				Link.InertialRotation = GenerateRotators(Rot);
			}
			
		}
		if (Inertial->FirstChildElement("mass"))
		{
			const FString MassStr = Inertial->FirstChildElement("mass")->Attribute("value");
			Link.InertialMass = FCString::Atof(*MassStr);
		}
	}

	return true;
}

bool UURDFParser::ParseJoint(tinyxml2::XMLElement * El, FJoint &Joint)
{
	//-----REQUIRED FOR URDF---------------
	Joint.Name = El->Attribute("name");
	FString JointType = El->Attribute("type");
	Joint.ParentLinkName = El->FirstChildElement("parent")->Attribute("link");
	Joint.ChildLinkName = El->FirstChildElement("child")->Attribute("link");

	if (Joint.Name.IsEmpty() || JointType.IsEmpty() || Joint.ParentLinkName.IsEmpty() || Joint.ChildLinkName.IsEmpty())
	{
		UE_LOG(LogTemp, Error, TEXT("Missing Data from Robot Joint - Parse Unsuccessful"));
		return false;
	}

	Joint.JointType = ProcessJointType(JointType);

	if (Joint.JointType == EJointType::UNKNOWN)
	{
		UE_LOG(LogTemp, Error, TEXT("Could not Parse Type of Joint, parsing unsucessful"));
		return false;
	}

	//------END REQUIRED-------------------

	//Optional Parameters - Default URDF Values are in the constructor
	const auto Origin = El->FirstChildElement("origin");
	if (Origin)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Parsing Origin Stuff"));
		if (Origin->Attribute("xyz"))
		{
			FString Xyz = Origin->Attribute("xyz");
			//URDF transition to UEF has a location offset of 100%
			auto NewLocation = GenerateVectors(Xyz);
			NewLocation.X *= 100.f;
			NewLocation.Y *= 100.f;
			NewLocation.Z *= 100.f;
			Joint.JointLocation = NewLocation;
		}
		if (Origin->Attribute("rpy"))
		{
			FString Rpy = Origin->Attribute("rpy");
			Joint.JointRotator = GenerateRotators(Rpy);
		}
	}

	//Checking the existence of the XML element first prevents UE4 from segfaulting
	const auto Axis = El->FirstChildElement("axis");
	if (Axis)
	{
		//UE_LOG(LogTemp, Warning, TEXT("Parsing Axis Stuff"));
		if (Axis->Attribute("xyz"))
		{
			FString Xyz = Axis->Attribute("xyz");
			Joint.JointAxis = GenerateVectors(Xyz);
		}
	}
	return true;
}

//Scale is dependent on type, so calculate these at the sime time
TPair<EMeshType, FVector> UURDFParser::SetMeshAndScale(tinyxml2::XMLElement * E)
{
	auto Type = EMeshType::UNKNOWN;
	FVector VectorScale(1.f);
	auto Geometry = E->FirstChildElement("geometry");
	if (Geometry)
	{
		if (Geometry->FirstChildElement("box"))
		{
			Type = EMeshType::BOX;
			FString Scale = Geometry->FirstChildElement("box")->Attribute("size");
			VectorScale = UURDFParser::GenerateVectors(Scale);
		}

		if (Geometry->FirstChildElement("cylinder"))
		{
			Type = EMeshType::CYLINDER;
			FString Radius = Geometry->FirstChildElement("cylinder")->Attribute("radius");
			FString Length = Geometry->FirstChildElement("cylinder")->Attribute("length");
			VectorScale = GenerateCylinderScale(Radius, Length);
		}

		if (Geometry->FirstChildElement("sphere"))
		{
			Type = EMeshType::SPHERE;
			FString Radius = Geometry->FirstChildElement("sphere")->Attribute("radius");
			VectorScale = GenerateSphereScale(Radius);
		}

		if (Geometry->FirstChildElement("mesh"))
		{
			Type = EMeshType::MESH;
			//Vector Scale is Optional Here, so let's make sure it exists...
			if (Geometry->FirstChildElement("mesh")->Attribute("scale"))
			{
				FString Scale = Geometry->FirstChildElement("mesh")->Attribute("scale");
				VectorScale = UURDFParser::GenerateVectors(Scale);
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Link Mesh Parameters could not be properly parsed"));
	}
	TPair<EMeshType, FVector> VisMapToReturn;
	VisMapToReturn.Key = Type;
	VisMapToReturn.Value = VectorScale;
	return VisMapToReturn;
}

EJointType UURDFParser::ProcessJointType(FString &S)
{
	if (S == "revolute")
	{
		return EJointType::REVOLUTE;
	}
	else if (S == "continuous")
	{
		return EJointType::CONTINUOUS;
	}
	else if (S == "prismatic")
	{
		return EJointType::PRISMATIC;
	}
	else if (S == "floating")
	{
		return EJointType::FLOATING;
	}
	else if (S == "planar")
	{
		return EJointType::PLANAR;
	}
	else if (S == "fixed")
	{
		return EJointType::FIXED;
	}
	return EJointType::UNKNOWN;

}


bool UURDFParser::ParseURDF()
{
	Owner = Cast<AROSRobot>(GetOwner());
	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("This Module does not have an appropriate Actor Class"));
		return false;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Name of actor = %s"), *Owner->GetName());
	}
	

	tinyxml2::XMLDocument XmlDoc;
	std::string Fn = TCHAR_TO_UTF8(*URDF_File.FilePath);

	//For this prototype, we shall only consider pure urdf. xacro has a library in ros to convert .xarcro.urdf -> .urdf
	if (Fn.substr(Fn.find_last_of(".") + 1) != "urdf") {
		UE_LOG(LogTemp, Error, TEXT("The specified file is not valid: %s"), *URDF_File.FilePath);
		return false;
	}

	XmlDoc.LoadFile(Fn.c_str());

	auto RobotXml = XmlDoc.FirstChildElement("robot");
	if (!RobotXml)
	{
		UE_LOG(LogTemp, Error, TEXT("No Robot Tag Detected - Is this a valid URDF?"));
		return false;
	}

	FString RobotName = RobotXml->Attribute("name");
	//UE_LOG(LogTemp, Warning, TEXT("Robot Name is: %s"), *RobotName);
	Owner->RobotName = RobotName;

	for (auto LinkXml = RobotXml->FirstChildElement("link"); LinkXml; LinkXml = LinkXml->NextSiblingElement("link"))
	{
		FLink Link;
		if (ParseLink(LinkXml, Link))
		{
			Owner->Links.Add(Link);
		}

	}

	for (auto JointXml = RobotXml->FirstChildElement("joint"); JointXml; JointXml = JointXml->NextSiblingElement("joint"))
	{
		FJoint Joint;
		if (ParseJoint(JointXml, Joint))
		{
			Owner->Joints.Add(Joint);
		}
	}

	return true;
}

// Called when the game starts
void UURDFParser::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UURDFParser::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

