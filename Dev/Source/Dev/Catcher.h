// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ROSRobot.h"
#include "ROSIntegration/Classes/RI/Topic.h"
#include "ROSIntegration/Classes/ROSIntegrationGameInstance.h"
#include "ROSIntegration/Public/std_msgs/String.h"
#include "ROSIntegration/Public/geometry_msgs/Vector3.h"
#include "ROSIntegration/Public/std_msgs/Float32MultiArray.h"
#include "Json.h"
#include "Catcher.generated.h"

class AROSRobot;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEV_API UCatcher : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCatcher();

	UFUNCTION(BlueprintCallable, Category = "CatchMechanic")
	void CanCatch(const FTransform &Transform, bool &Possible);

	UFUNCTION(BlueprintCallable, Category = "CatchMechanic")
	void ProcessCatch(const FString &Data) const;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
private:
	UPROPERTY()
	UTopic *CatcherPublisher_;

	UPROPERTY()
	UTopic *CatcherSubscriber_;

	UPROPERTY()
	UROSIntegrationGameInstance *RosInstance_;

	UPROPERTY()
	AROSRobot * Owner_;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool SolutionAvailable_ = false;

	void InitPublisher();
	void InitSubscriber();

	

};
