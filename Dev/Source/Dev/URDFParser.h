// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Core/Public/Misc/FileHelper.h"
#include "Runtime/Core/Public/Misc/Paths.h"
#include "Runtime/Core/Public/UObject/NameTypes.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "Components/ActorComponent.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Runtime/Core/Public/Containers/UnrealString.h"

#include "tinyxml2.h"
#include "Joint.h"
#include "Link.h"
#include "URDFParser.generated.h"


class AROSRobot;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DEV_API UURDFParser : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UURDFParser();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	

public:
	static TPair<EMeshType, FVector> SetMeshAndScale(tinyxml2::XMLElement * E);
	static EJointType ProcessJointType(FString & S);
	static FVector GenerateCylinderScale(FString &Rad, FString &Len);
	static FVector GenerateSphereScale(FString &Rad);
	static bool ParseLink(tinyxml2::XMLElement * El, FLink & Link);
	static bool ParseJoint(tinyxml2::XMLElement * El, FJoint & Joint);
	static FVector GenerateVectors(FString &S);
	static FRotator GenerateRotators(FString &S);
	bool ParseURDF();
	AROSRobot * Owner;
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	//Allows us to set a file explorer prompt in UE4
	UPROPERTY(EditAnywhere, meta = (RelativePath))
	FFilePath URDF_File;


	
};
