// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Runtime/Core/Public/Containers/Map.h"
#include "Runtime/Core/Public/Containers/Array.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsHandleComponent.h"

#include "Link.h"
#include "Joint.h"
#include "RobotLink.h"
#include "RobotJoint.h"
#include "Catcher.h"
#include "URDFParser.h"


#include "ROSRobot.generated.h"


USTRUCT(BlueprintType)
struct FRobNode
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLink Link;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FJoint Joint;

	FRobNode * Parent = nullptr;
	TArray<FRobNode *> Children;
	bool IsBaseLink = false;
	bool IsEndLink = false;

};

UCLASS()
class DEV_API AROSRobot : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AROSRobot();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable, Category = "CatchMechanic")
	bool RotateJoint(const FString& Name, const float TargetRotation) const;


	//Tree Structure Components
	FRobNode * BaseNode = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	FString RobotName;

	TArray<FLink> Links;
	
	TArray<FJoint> Joints;

	//Used for mesh generation
	TMap<FString, class UPhysicsConstraintComponent*> JointComponents;

	TMap<FString, class UPrimitiveComponent*> LinkComponents;

	/*UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<FString, FQuat> OriginRotations;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TMap<FString, FVector> OriginLocations;*/

	//Helper Functions for Mesh generation
	UStaticMesh* CylinderMesh;
	UStaticMesh* CubeMesh;
	UStaticMesh* SphereMesh;

	//The tree traversal is dealt from a top down manner
	// So the trees base == hand
	// And the end branch == base
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	FString BaseLink;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	FString EndLink;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UPhysicsHandleComponent * PhysicsHandleComponent = nullptr;
	

private:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	USceneComponent * Root_;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UURDFParser* Parser_;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UCatcher* Catcher_;

	void BuildTree(FRobNode * Node);
	bool BuildRobot();
	bool RenderModel(FRobNode * Node);

	static FConstraintInstance SetConstraintInstance(FJoint * Joint);


};
