// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Core/Public/Containers/UnrealString.h"
#include "Runtime/Core/Public/Math/Vector.h"
#include "Runtime/Core/Public/Math/Rotator.h"
#include "Joint.generated.h"



UENUM(BluePrintType)
enum class EJointType : uint8 {
	REVOLUTE UMETA(DisplayName = "Revolute"), 
	CONTINUOUS UMETA(DisplayName = "Continuous"), 
	PRISMATIC UMETA(DisplayName = "Prismatic"), 
	FLOATING UMETA(DisplayName = "Floating"), 
	PLANAR UMETA(DisplayName = "Planar"), 
	FIXED UMETA(DisplayName = "Fixed"), 
	UNKNOWN = 255 UMETA(DisplayName = "Unknown"),
};


//http://wiki.ros.org/urdf/XML/joint for specification of Joint Componnt in URDF Joint
USTRUCT(BlueprintType)
struct FJoint
{
	GENERATED_BODY()

	//REQUIRED 
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString ParentLinkName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString ChildLinkName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		EJointType JointType = EJointType::UNKNOWN;


	//OPTIONAL WITH DEFAULTS
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FRotator JointRotator = FRotator(0.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector JointLocation = FVector(0.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector JointAxis = FVector(1, 0, 0);

		
	//Required for prismatic and revolute
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float LimitUpper = 0.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float LimitLower = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float LimitEffort;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float LimitVelocity;


	//Saftey Controller Methods - Entirely Optionial
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SoftLowerLimit = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float SoftUpperLimit = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float KPosition = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float KVelocity;




};


