#include <boost/date_time.hpp>
#include <trac_ik/trac_ik.hpp>
#include <ros/ros.h>
#include <urdf/model.h>
#include <std_msgs/String.h>
#include <geometry_msgs/Vector3.h>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <jsoncpp/json/json.h>
#include <iostream>
#include <exception>


class BallCatcher
{
  public:
    BallCatcher(ros::NodeHandle &nh, std::string cs, std::string ce, double t, std::string u, const std::string &robot_name);
    void checkForSolution(const geometry_msgs::Vector3 &msg);
  private:
    std::string chain_start;
    std::string chain_end;
    std::string urdf_param;
    TRAC_IK::TRAC_IK * solver;

    KDL::Chain chain;
    KDL::JntArray ll, ul, nominal;
    double eps = 1e-5;
    double timeout;

    ros::Publisher catch_publisher;
    ros::Subscriber sub;

    // Json::Reader reader;
};